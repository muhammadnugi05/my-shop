TUGAS 10 - SANBERCODE MY SQL

1. Membuat database

create database myshop;

2. Membuat tabel di dalam database
[user]
create table user(
    -> id int(8) auto_increment,
    -> primary key(id),
    -> name varchar(255),
    ->  email varchar(255),
    -> password varchar(255)
    -> );
    
[items]
create table items(
    -> id int(8) auto_increment,
    -> primary key(id),
    -> name varchar(225),
    -> description varchar(225),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id)
    -> );

[categories]
create table categories(
    -> id int(8) auto_increment,
    -> name varchar(225),
    -> primary key(id)
    -> );

3. Memasukkan data pada tabel
[user]
insert into user (name, email, password) values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jeneta123");

[items]
insert into items (name, description, price, stock, category_id) values ("Sumsang b50","hape keren dari merek sumsang","4000000","100",1),("Uniklooh","baju keren dari brand ternama","500000","50",2), ("IMHO Watch","jam tangan anak yang jujur banget","2000000","10",1);

[categories]
insert into categories (name) values("gedget"),("cloth"),("men"),("women"),("branded");

4. Mengambil data dari database
[a. Mengambil data users]
select id,name,email from user;

[b. Mengambil data items]
select *from items where price>1000000;

[c. Menampilkan data items join dengan kategori]
select items.name,items.description,items.price, items.stock, items.category_id,categories.name from items inner join categories on items.category_id=categories.id;

quiz
select customers.name as customers_name, sum(amount) as total_amount from customers inner join orders on customers.id=orders.customer_id group by customer_id;


select *from items where name like 'unik%';

5. Mengubah data dari database
update items SET price = 2500000 where id=1;
